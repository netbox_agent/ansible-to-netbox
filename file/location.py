#!/usr/bin/env python3
import subprocess

# Nom du fichier CSV de sortie
output_file = "/opt/netbox_venv/location.csv"

#exec lldpd
lldpd_raw = subprocess.run(["lldpcli", "show", "neighbors"], capture_output=True, text=True)


# Get the hostname of the switch from the lldp output after 'SysName:'
switch_name = lldpd_raw.stdout.split('ChassisID:')[1].split('\n')[0].strip()

# Check if the switch name is equal to a specific value
if "po04" in switch_name.lower():
    rack = "P04"
    site = "GDX"    
elif "po05" in switch_name.lower():
    rack = "P05"
    site = "GDX"
elif "po06" in switch_name.lower():
    rack = "P06"
    site = "GDX"
elif "racka" in switch_name.lower():
    rack="HQ-Rack-A"
    site="hq-stm"
elif "rackb" in switch_name.lower():
    rack="HQ-Rack-B"
    site="hq-stm"
elif "rackc" in switch_name.lower() or "office" in switch_name.lower() or "sp01-stm1" in switch_name.lower():
    rack="HQ-Rack-C"
    site="hq-stm"
elif "stm2" in switch_name.lower():
    rack="BAIE1"
    site="sgt-stm"
elif "pa3par1" in switch_name.lower():
    rack="EQX-PA3-SIPARTECH-01"
    site="eqx-pa3"
elif "th2par" in switch_name.lower():
    rack="11L3"
    site="th2"
elif "eqd" in switch_name.lower():
    rack="eqd"
    site="eqd"
else:
    rack = "Unknown"
    site = "Unknown"

# Save the site and rack in the CSV file
with open(output_file, 'w') as f:
    f.write(f"{site}\n")
    f.write(f"{rack}\n")

