#!/usr/bin/env python3
import subprocess

# Nom du fichier CSV de sortie
output_file = "/opt/netbox_venv/nic.csv"

# Récupère la liste des interfaces réseau
interfaces_raw = subprocess.run(["ip", "-o", "link", "show"], capture_output=True, text=True)
interfaces = [line.split(': ')[1] for line in interfaces_raw.stdout.strip().splitlines()]

# Vérifie si le fichier CSV existe déjà et le supprime pour éviter l'ajout multiple
try:
    with open(output_file, 'w') as f:
        pass
except FileNotFoundError:
    pass

# Boucle à travers chaque interface
for interface in interfaces:
    # Récupère le nom de l'interface
    interface_name = interface

    # Récupère l'adresse IP de l'interface
    ip_raw = subprocess.run(["ip", "-o", "addr", "show", "dev", interface], capture_output=True, text=True)
    ip_address = next((line.split()[3] for line in ip_raw.stdout.strip().splitlines() if line.split()[2] == "inet"), None)

    # Récupère l'adresse MAC de l'interface
    mac_raw = subprocess.run(["ip", "-o", "link", "show", interface], capture_output=True, text=True)
    mac_address = mac_raw.stdout.strip().split()[16] if mac_raw.stdout else None

    # Récupère la vitesse de l'interface (en Mbps)
    speed_raw = subprocess.run(["ethtool", interface], capture_output=True, text=True)
    speed = next((line.split()[1] for line in speed_raw.stdout.strip().splitlines() if "Speed:" in line), None)
    speed = ''.join(filter(lambda x: x.isdigit(), speed)) if speed else None

    # Si la vitesse n'est pas disponible ou est 0, passer à l'interface suivante
    if not speed or int(speed) == 0:
        continue

    # Remplace les valeurs spécifiques de vitesse par leur équivalent texte
    if int(speed) == 1000:
        speed = "1000base-T"
    elif int(speed) == 10000:
        speed = "10gbase-T"
    else:
        speed = "other"


    # Formatage pour CSV (échappe les virgules dans les valeurs)
    interface_name = interface_name.replace(',', '\\,')
    ip_address = ip_address.replace(',', '\\,') if ip_address else ""
    mac_address = mac_address.replace(',', '\\,') if mac_address else ""

    # Écriture dans le fichier CSV
    with open(output_file, 'a') as f:
        f.write(f"{interface_name},{ip_address},{mac_address},{speed}\n")
